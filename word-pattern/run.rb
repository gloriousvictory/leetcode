# @param {String} pattern
# @param {String} str
# @return {Boolean}
def word_pattern(pattern, str)
  str_words = str.split(" ")

  return false if pattern.length != str_words.length

  holder_hash = {}

  str_words.each_with_index do |word, index|
    pattern.chars[index]
  end

  true

end
