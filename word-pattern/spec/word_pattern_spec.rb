require "./run.rb"

RSpec.describe "word_pattern" do

  context "for abba and dog cat cat dog" do
    it "is true" do
      result = word_pattern("abba", "dog cat cat dog")

      expect(result).to equal(true)
    end
  end

  context "for abba and dog cat cat fish" do
    it "is false" do
      result = word_pattern("abba", "dog cat cat fish")

      expect(result).to equal(false)
    end
  end

  context "for aaaa and dog cat cat dog" do
    it "is false" do
      result = word_pattern("aaaa", "dog cat cat dog")

      expect(result).to equal(false)
    end
  end

  context "for abba and dog dog dog dog" do
    it "is false" do
      result = word_pattern("abba", "dog dog dog dog")

      expect(result).to equal(false)
    end
  end

end
