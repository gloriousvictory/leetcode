require "./run.rb"

RSpec.describe "is_isomorphic" do

  context "for egg and add" do
    it "is true" do
      result = is_isomorphic("egg", "add")

      expect(result).to equal(true)
    end
  end

  context "for foo and bar" do
    it "is false" do
      result = is_isomorphic("foo", "bar")

      expect(result).to equal(false)
    end
  end

  context "for fool and aard" do
    it "is false" do
      result = is_isomorphic("fool", "aard")

      expect(result).to equal(false)
    end
  end

  context "for title and paper" do
    it "is true" do
      result = is_isomorphic("title", "paper")

      expect(result).to equal(true)
    end
  end

  context "for cat and dog" do
    it "is true" do
      result = is_isomorphic("cat", "dog")

      expect(result).to equal(true)
    end
  end

  context "for longstringgggggggggg and shrtstr" do
    it "is false" do
      result = is_isomorphic("longstringgggggggggg", "shrtstr")

      expect(result).to equal(false)
    end
  end

end
