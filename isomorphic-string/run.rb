# @param {String} s
# @param {String} t
# @return {Boolean}

def is_isomorphic(s, t)
  return false if s.length != t.length

  s_map = s.chars.uniq
  t_map = t.chars.uniq

  (0...s.length).each do |i|
    unless s_map.index(s[i]) == t_map.index(t[i])
      return false
    end
  end

  true

end
